package arrays;

import java.util.Comparator;

public class CountryISO implements Comparable<CountryISO>, Comparator<CountryISO> {

    private String countryISO;
    private String countryName;

    public CountryISO(String countryISO, String countryName) {
        this.countryISO = countryISO;
        this.countryName = countryName;
    }

    @Override
    public int compareTo(CountryISO countryISO) {
        return this.countryISO.length() - countryISO.countryISO.length();
    }

    @Override
    public int compare(CountryISO o1, CountryISO o2) {
        return o1.countryName.length() - o2.countryName.length();
    }


    @Override
    public String toString() {
        return "Country ISO - " + countryISO + ", country name - " + countryName;
    }
}
