package arrays;

public class Container {

    private int size;
    private int length = 10;

    private String[] stringArray;

    public Container() {
        stringArray = new String[length];
    }

    public void addString(String string) {
        if (size < stringArray.length) {
            stringArray[size] = string;
            size++;
        } else {
            stringArray = copy();
            addString(string);
        }
    }

    public String getString(int index) {
        if ((index < size) && (index >= 0)) {
            return stringArray[index];
        } else {
            System.out.println("Try again");
            return "";
        }
    }

    private String[] copy() {
        length += 5;
        String[] temp = new String[length];
        System.arraycopy(stringArray, 0, temp, 0, stringArray.length);
        return temp;
    }
}
