package arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Generator {

    public List<CountryISO> getList() {
        ArrayList<CountryISO> countryISOS = new ArrayList<>();
        String[] locales = Locale.getISOCountries();
        for (String countryCode : locales) {
            Locale locale = new Locale("", countryCode);
            countryISOS.add(new CountryISO(locale.getCountry(), locale.getDisplayCountry()));
        }
        return countryISOS;
    }


}
