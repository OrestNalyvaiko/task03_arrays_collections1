package com.nalyvaiko.generics;

import com.nalyvaiko.generics.fruits.Fruit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Shop {
    List<Fruit> fruitsList = new ArrayList<>();

    public void putFruits(List <? extends Fruit> fruits){
        for (Fruit fruit: fruits) {
            Collections.addAll(fruitsList,fruit);
        }
    }

    public void getFruits(List<? super Fruit> fruits){
        for (Fruit fruit: fruitsList) {
            Collections.addAll(fruits,fruit);
        }
    }
}
