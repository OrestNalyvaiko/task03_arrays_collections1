package com.nalyvaiko.generics.fruits;

public class Fruit {
    @Override
    public String toString(){
        return getClass().getSimpleName();
    }
}
