package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    private static Deque<Integer> deque = new Deque<>();

    public static void main(String[] args) {
        dequeOperations();
        containersComparing();
        sorting();
    }

    private static void sorting() {
        Generator generator = new Generator();
        List<CountryISO> countryISOS = generator.getList();

        Collections.sort(countryISOS);
        for (CountryISO countries : countryISOS) {
            System.out.println(countries);
        }
    }

    private static void containersComparing() {
        List<String> arrayList = new ArrayList<>();
        int size = 100000;
        long nanos = System.nanoTime();
        for (int i = 0; i < size; i++) {
            arrayList.add("Hello");
        }
        System.out.println("ArrayList Time:" + (System.nanoTime() - nanos) + " ns");
        Container container = new Container();
        nanos = System.nanoTime();
        for (int i = 0; i < size; i++) {
            container.addString("Hello");
        }
        System.out.println("Container Time:" + (System.nanoTime() - nanos) + " ns");
    }

    private static void dequeOperations() {
        add();
        showData();
        offer();
        showData();
        peekFirst();
        peekLast();
        pollFirst();
        pollLast();
        showData();
        getFirst();
        getLast();
        removeFirst();
        removeLast();
        showData();
    }

    private static void add() {
        deque.addFirst(10);
        deque.addFirst(11);
        deque.addFirst(12);
        deque.addLast(8);
    }

    private static void showData() {
        System.out.println("Data in deque ");
        for (Integer integer : deque) {
            System.out.print(integer + ", ");
        }
        System.out.println();
    }

    private static void offer() {
        deque.offerLast(5);
        deque.offerFirst(9);
    }

    private static void peekFirst() {
        System.out.println("First element of the deque after peekFirst method "
                + deque.peekFirst());
    }

    private static void peekLast() {
        System.out.println("Last element of the deque after peekLast method "
                + deque.peekLast());
    }

    private static void getFirst() {
        System.out.println("First element of the deque after getFirst method "
                + deque.getFirst());
    }

    private static void getLast() {
        System.out.println("Last element of the deque after getLast method "
                + deque.getLast());
    }

    private static void pollFirst() {
        System.out.println("First element of the deque after pollFirst method "
                + deque.pollFirst());
    }

    private static void pollLast() {
        System.out.println("Last element of the deque after pollLast method "
                + deque.pollLast());
    }

    private static void removeFirst() {
        System.out.println("First element of the deque after removeFirst method "
                + deque.removeFirst());
    }

    private static void removeLast() {
        System.out.println("Last element of the deque after removeLast method "
                + deque.removeLast());
    }
}
