package com.nalyvaiko.generics;

import com.nalyvaiko.generics.fruits.Apple;
import com.nalyvaiko.generics.fruits.Fruit;
import com.nalyvaiko.generics.fruits.Orange;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

    public static void main(String[] args) {
        priorityOperations();
        System.out.println();
        usingWildcardsExample();


    }

    private static void usingWildcardsExample(){
        Shop shop = new Shop();

        List<Fruit> fruits = new ArrayList<>();
        fruits.add(new Fruit());
        fruits.add(new Apple());
        fruits.add(new Orange());
        shop.putFruits(fruits);
        shop.getFruits(fruits);
        for (Fruit fruit: fruits ) {
            System.out.println(fruit);
        }
    }
    private static void priorityOperations() {
        add();
        showData();
        clear();
        showData();
        add();
        showSize();
        poll();
        showSize();
        peek();
        offer();
        showData();
        toArray();
    }

    private static void add() {
        priorityQueue.add(0);
        priorityQueue.add(1);
        priorityQueue.add(2);
        priorityQueue.add(3);
        priorityQueue.add(4);
        priorityQueue.add(5);
        priorityQueue.add(6);
        priorityQueue.add(7);
        priorityQueue.add(8);
        priorityQueue.add(9);
        priorityQueue.add(10);
    }

    private static void clear() {
        priorityQueue.clear();
        System.out.println("Priority queue after clear method ");
    }

    private static void showSize() {
        System.out.println("Priority queue size " + priorityQueue.size());
    }

    private static void peek() {
        System.out.println("Heap of  the queue after peek method "
                + priorityQueue.peek());
    }

    private static void poll() {
        System.out.println("Heap of  the queue after poll method "
                + priorityQueue.poll());
    }

    private static void offer() {
        priorityQueue.offer(1);
    }

    private static void showData() {
        System.out.println("Data in priority queue ");
        for (Integer integer : priorityQueue) {
            System.out.print(integer + ", ");
        }
        System.out.println();
    }

    private static void toArray() {
        System.out.println("Using toArray method ");
        for (Object o : priorityQueue.toArray()) {
            System.out.print(o + ", ");
        }
    }
}
